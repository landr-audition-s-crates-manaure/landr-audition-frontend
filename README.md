## Pre-requisities

1. **Node version: 16.14.0**
2. **NPM version: 7.24.0**
3. run backend project on [http://localhost:8000](http://localhost:8000)

## Execute project

Please follow these steps:

1. **npm install**.
2. **npm start**
3. Open [http://localhost:3000](http://localhost:3000) to view it in your browser.