import React, { useState } from "react";
import ContactList from "./components/ContactList";
import ContactForm from "./components/ContactForm";
import ContactProvider from "./context/ContactsContext";

function App() {
  const [showForm, setShowForm] = useState(false);
  return (
    <ContactProvider>
      {!showForm ? <ContactList setShowForm={setShowForm}></ContactList> : null}
      {showForm ? <ContactForm setShowForm={setShowForm}></ContactForm> : null}
    </ContactProvider>
  );
}

export default App;
