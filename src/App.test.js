import { render, screen } from '@testing-library/react';
import App from './App';

test('renders contact list', () => {
  render(<App />);
  const linkElement = screen.getByText(/Contact List/i);
  expect(linkElement).toBeInTheDocument();
});
