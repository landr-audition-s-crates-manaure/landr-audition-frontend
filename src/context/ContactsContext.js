import axios from "axios";
import React, { createContext, useState, useEffect } from "react";

export const ContactContext = createContext();

const ContactProvider = (props) => {
  const [contacts, setContacts] = useState([]);
  const [selectedContact, setSelectedContact] = useState(null);

  useEffect(() => {
    const getContactList = async () => {
      const url = "http://localhost:8000/api/contacts";
      const contactsFromApi = await axios(url);
      setContacts(contactsFromApi.data);
    };
    getContactList();
  }, []);

  const updateContactsList = async () => {
    const url = "http://localhost:8000/api/contacts";
    const contactsFromApi = await axios(url);
    setContacts(contactsFromApi.data);
  };

  const saveContact = async () => {
    let url = "http://localhost:8000/api/contacts";
    if (selectedContact.id > 0) {
      url = `${url}/${selectedContact.id}`;
    }
    try {
      // eslint-disable-next-line no-unused-vars
      const response = await axios.post(url, selectedContact);
      await updateContactsList();
      return true;
    } catch (error) {
      console.error(error);
      return false;
    }
  };

  const deleteContact = async (id) => {
    let url = `http://localhost:8000/api/contacts/${id}`;
    try {
      // eslint-disable-next-line no-unused-vars
      const response = await axios.delete(url);
      await updateContactsList();
      return true;
    } catch (error) {
      console.error(error);
      return false;
    }
  };

  return (
    <ContactContext.Provider
      value={{ contacts, selectedContact, setSelectedContact, saveContact, deleteContact }}
    >
      {props.children}
    </ContactContext.Provider>
  );
};

export default ContactProvider;
