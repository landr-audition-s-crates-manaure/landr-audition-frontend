import { render, screen } from "@testing-library/react";
import ContactForm from "./components/ContactForm";
import ContactProvider from "./context/ContactsContext";

test("renders contact form", () => {
  const setShowForm = jest.fn()
  render(
    <ContactProvider>
      <ContactForm setShowForm={setShowForm} />
    </ContactProvider>
  );
  const linkElement = screen.getByText(/Contact Form/i);
  expect(linkElement).toBeInTheDocument();
});
