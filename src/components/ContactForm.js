/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useState, useEffect } from "react";
import { ContactContext } from "../context/ContactsContext";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { Alert } from "@material-ui/lab";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  listContainer: {
    padding: "25px",
    background: theme.palette.background.paper,
    margin: "15px",
    borderRadius: "15px",
  },
  field: {
    marginBottom: "15px",
    marginTop: "10px",
  },
}));

const ContactForm = ({ setShowForm }) => {
  const classes = useStyles();
  const { selectedContact, setSelectedContact, saveContact } = useContext(ContactContext);
  const [name, setName] = useState(selectedContact ? selectedContact.name : '');
  const [jobTitle, setJobTitle] = useState(selectedContact ? selectedContact.job_title : '');
  const [address, setAddress] = useState(selectedContact ? selectedContact.address : '');
  const [phone, setPhone] = useState(selectedContact ? selectedContact.phone : '');
  const [email, setEmail] = useState(selectedContact ? selectedContact.email : '');
  const [error, setError] = useState("");
  const [submitForm, setSubmitForm] = useState(false)
  useEffect(async () => {
    if(submitForm){
        const formsubmitted = async () => {
            const success = await saveContact();
            return success;
        }
        const response = await formsubmitted();
        if(response){
            setShowForm(false);
        }else{
            setError('Error submitting form, please try again');
        }
    }
  }, [submitForm])
  
  const validateSubmit = async (e) => {
    e.preventDefault();
    setError('');
    setSubmitForm(false);
    if (name === "") {
      setError("Field name is required");
      return;
    }
    if (jobTitle === "") {
      setError("Field job title is required");
      return;
    }
    if (address === "") {
      setError("Field address is required");
      return;
    }
    if (phone === "") {
      setError("Field phone is required");
      return;
    }
    if (email === "") {
      setError("Field email is required");
      return;
    }
    setSelectedContact({
        id: selectedContact.id,
        name,
        job_title: jobTitle,
        address,
        phone,
        email
    });
    setSubmitForm(true);
  };
  return (
    <div>
      <Grid item xs={12} md={12}>
        <h1>
          Contact Form
          <Button onClick={() => setShowForm(false)} color="primary">
            Back to list
          </Button>
        </h1>
        <div className={classes.listContainer}>
          <form onSubmit={validateSubmit}>
            <h2>Please fill up form</h2>
            {error.length > 0 ? <Alert severity="error">{error}</Alert> : null}
            <div className={classes.field}>
              <TextField
                fullWidth
                id="name"
                label="Name"
                variant="outlined"
                value={name}
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
            </div>
            <div className={classes.field}>
              <TextField
                fullWidth
                id="jobtitle"
                label="Job Title"
                variant="outlined"
                value={jobTitle}
                onChange={(e) => {
                  setJobTitle(e.target.value);
                }}
              />
            </div>
            <div className={classes.field}>
              <TextField
                fullWidth
                id="address"
                label="Address"
                variant="outlined"
                value={address}
                onChange={(e) => {
                  setAddress(e.target.value);
                }}
              />
            </div>
            <div className={classes.field}>
              <TextField
                fullWidth
                id="phone"
                label="Phone"
                variant="outlined"
                value={phone}
                onChange={(e) => {
                  setPhone(e.target.value);
                }}
              />
            </div>
            <div className={classes.field}>
              <TextField
                fullWidth
                id="email"
                label="E-mail"
                variant="outlined"
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
            </div>
            <Button
              onClick={(e) => validateSubmit(e)}
              fullWidth
              variant="contained"
              color="primary"
            >
              SUBMIT
            </Button>
          </form>
        </div>
      </Grid>
    </div>
  );
};

export default ContactForm;
