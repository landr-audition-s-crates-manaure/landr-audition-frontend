/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  listContainer: {
    padding: "25px",
    background: theme.palette.background.paper,
    margin: "15px",
    borderRadius: "15px",
  },
  field: {
    marginBottom: "15px",
    marginTop: "10px",
  },
}));

const ContactDetail = ({ contact, setShowDetail }) => {
  const classes = useStyles();
  
  return (
    <div>
      <Grid item xs={12} md={12}>
        <h1>
          Contact details
          <Button onClick={() => setShowDetail(false)} color="primary">
            Back to list
          </Button>
        </h1>
        <div className={classes.listContainer}>
          <p><strong>Name: </strong>{contact.name}</p>
          <p><strong>Job title: </strong>{contact.job_title}</p>
          <p><strong>Address: </strong>{contact.address}</p>
          <p><strong>Phone: </strong>{contact.phone}</p>
          <p><strong>E-mail: </strong>{contact.email}</p>
        </div>
      </Grid>
    </div>
  );
};

export default ContactDetail;
