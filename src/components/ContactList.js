import React, { useContext, useState } from "react";
import { ContactContext } from "../context/ContactsContext";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import ContactsIcon from "@material-ui/icons/Contacts";
import DeleteIcon from "@material-ui/icons/Delete";
import CreateIcon from "@material-ui/icons/Create";
import RemoveRedEyeIcon from "@material-ui/icons/RemoveRedEye";
import ContactDetail from "./ContactDetail";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  listContainer: {
    padding: "25px",
    background: theme.palette.background.paper,
    margin: "15px",
    borderRadius: "15px",
  },
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const ContactList = ({ setShowForm }) => {
  const classes = useStyles();
  const { contacts, setSelectedContact, deleteContact } =
    useContext(ContactContext);
  const [showDetail, setShowDetail] = useState(false);
  const [open, setOpen] = useState(false);
  const [id, setId] = useState(0);
  const [contactDet, setContactDet] = useState(null);

  const handleSelectedContact = (contact) => {
    setSelectedContact(contact);
    setShowForm(true);
  };
  const openModal = (idSelected) => {
    setId(idSelected);
    setOpen(true);
  };

  const handleDelete = async () => {
    // eslint-disable-next-line no-unused-vars
    const response = await deleteContact(id);
    setOpen(false);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleShowDetail = (contactDetail) => {
    setShowDetail(true);
    setContactDet(contactDetail)
  }

  return (
    <div>
      {!showDetail && (
        <div>
          <Grid item xs={12} md={12}>
            <h1>Contact List</h1>
            <div className={classes.listContainer}>
              <List dense={false}>
                {contacts.map((contact) => (
                  <ListItem key={contact.id}>
                    <ListItemAvatar>
                      <Avatar>
                        <ContactsIcon />
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={contact.name} secondary={null} />
                    <ListItemSecondaryAction>
                      <IconButton
                        edge="end"
                        aria-label="edit"
                        onClick={() => handleSelectedContact(contact)}
                      >
                        <CreateIcon />
                      </IconButton>
                      <IconButton
                        edge="end"
                        aria-label="detail"
                        onClick={() => handleShowDetail(contact)}
                      >
                        <RemoveRedEyeIcon />
                      </IconButton>
                      <IconButton edge="end" aria-label="delete">
                        <DeleteIcon onClick={() => openModal(contact.id)} />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                ))}
                {contacts.length === 0 ? (
                  <div>You don't have any contacts</div>
                ) : (
                  ""
                )}
              </List>
              <Button
                variant="contained"
                color="primary"
                onClick={() =>
                  handleSelectedContact({
                    id: 0,
                    name: "",
                    job_title: "",
                    address: "",
                    phone: "",
                    email: "",
                    created_at: null,
                    updated_at: null,
                  })
                }
              >
                ADD
              </Button>
            </div>
          </Grid>
          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">
              {"Are you sure about deleting this contact?"}
            </DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                If you delete this contact it won't be available anymore
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={() => setOpen(false)} color="primary">
                Disagree
              </Button>
              <Button onClick={handleDelete} color="primary">
                Agree
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      )}
      {
        showDetail && (<ContactDetail contact={contactDet} setShowDetail={setShowDetail}></ContactDetail>)
      }
    </div>
  );
};

export default ContactList;
